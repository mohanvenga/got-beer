package com.network.beer.demo;

    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.http.HttpStatus;
    import org.springframework.http.ResponseEntity;
    import org.springframework.web.bind.annotation.GetMapping;
    import org.springframework.web.bind.annotation.PathVariable;
    import org.springframework.web.bind.annotation.PostMapping;
    import org.springframework.web.bind.annotation.PutMapping;
    import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

    import java.util.List;
    import java.util.HashSet;

   @RestController
   public class BeerController {

       @Autowired
       private BeerRepository beerRepository;
       @Autowired
       private PersonRepository personRepository;

       @GetMapping("/beers")
       public ResponseEntity<List<Beer>> getAllCustomers() {
           List<Beer> customers = beerRepository.findAll();
           return new ResponseEntity<>(customers, HttpStatus.OK);
       }

        @PostMapping("/add")
        public ResponseEntity<String> createProduct(@RequestBody ProductRequest productRequest) {

            // need to validate API input.

            Beer beer = new Beer();
            beer.setName(productRequest.getName());
            beer.setPrice(productRequest.getPrice());
            beer.setScore(productRequest.getScore());
            beer.setAuthId(productRequest.getAuthId());

            beerRepository.save(beer);
            
            // need to provide better json response.
            return ResponseEntity.ok("Product created successfully!");
        }

        @PutMapping("/update/{id}")
        public ResponseEntity<String> updateProduct(@PathVariable Integer id, @RequestBody ProductRequest updatedProduct) {

            Beer beer  = beerRepository.getReferenceById(id);
            beer.setName(updatedProduct.getName());
            beerRepository.save(beer);

            return ResponseEntity.ok("Product updated successfully!");
        }

        @PostMapping("/register")
        public ResponseEntity<String> register(@RequestBody RegistrationRequest regRequest) {

            List<Person> users = personRepository.findAll();
            HashSet<String> user_emails = new HashSet<>();

            for (Person user : users) {
                String email = user.getEmail();
                if ( email != null && !email.isEmpty()) {
                    user_emails.add(email);
                }
            }

            String reg_email = regRequest.getEmail();
            if (user_emails.contains(reg_email)) {
                return ResponseEntity.ok("User already registered!");
            } else {

                String passcode = regRequest.getPassword();
                String name = regRequest.getName();

                Person user = new Person();
                user.setEmail(reg_email);
                user.setName(name);
                user.setPassword(passcode);

                personRepository.save(user);
                return ResponseEntity.ok("Registration successful");
            }

        }
        
        @PostMapping("/login")
        public ResponseEntity<String> login(@RequestBody LoginRequest loginRequest) {

            List<Person> users = personRepository.findAll();
            HashSet<String> user_emails = new HashSet<>();

            for (Person user : users) {
                String email = user.getEmail();
                if ( email != null && !email.isEmpty()) {
                    user_emails.add(email);
                }
            }

            String email = loginRequest.getEmail();
            String password = loginRequest.getPassword();

            if (user_emails.contains(email)) {
                for (Person user : users) {
                    if (user.getEmail() == email &&
                    user.getPassword() == password) {
                        return ResponseEntity.ok("Login successful");
                    }
                }
            }
            return ResponseEntity.ok("Invalid email or password.");
            
        }

        @GetMapping("/mybeers")
        public ResponseEntity<List<Beer>> getMyBeers(@RequestParam(required = false) String id) {
            List<Beer> customers = beerRepository.findAll();
            System.out.println(id);
            return new ResponseEntity<>(customers, HttpStatus.OK);
        }

   }
